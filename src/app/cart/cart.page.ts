import { Component, OnInit } from '@angular/core';
import { ActionSheetController, AlertController, ToastController } from '@ionic/angular';
import { Article } from '../models/articles';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {

  newArticle: string; // [(ngModel)]

  articles: Article[]; // *ngFor

  constructor(
    private asCtrl: ActionSheetController,
    private alertCtrl: AlertController,
    private toastrCtrl: ToastController,
  ) { }

  ngOnInit() { 
    this.articles = [];
  }

  ajouter() { // click()
    if(!this.newArticle.trim()) return;
    // this.articles.push(this.newArticle);
    this.articles = [...this.articles, { name: this.newArticle, isChecked: false }];
    this.newArticle = null;
    this.toastrCtrl.create({
      color: 'success',
      message: 'L\'article a été ajouté avec success',
      duration: 3000,
    }).then(t => t.present());
  }

  clearAll() {
    this.alertCtrl.create({ 
      header: 'Confirmation',
      backdropDismiss: false,
      buttons: [{
        text: 'Annuler'
      }, {
        text: 'Oui',
        handler: () => {
          this.articles = [];
          this.toastrCtrl.create({
            color: 'danger',
            message: 'Tous les articles ont été supprimés',
            duration: 3000,
          }).then(t => t.present());
        }
      }] 
    }).then(a => a.present());
  }

  openActionSheet(item) {
    this.asCtrl.create({
      header: 'Actions',
      buttons: [{
        text: 'Supprimer',
        icon: 'trash',
        role: 'destructive',
        handler: () => {
          // let i = this.articles.indexOf(item);
          // this.articles.splice(i, 1);
          this.articles = this.articles.filter(a => a !== item);
        }
      }, {
        text: 'Cocher',
        icon: 'checkmark',
        handler: () => item.isChecked = true
      }]
    }).then(as => as.present());
  }


  // async openActionSheet() {
  //   let as =  await this.asCtrl.create({
  //     header: 'Actions',
  //     buttons: [{
  //       text: 'Supprimer'
  //     }, {
  //       text: 'Cocher'
  //     }]
  //   });
  //   as.present();
  // }

}
