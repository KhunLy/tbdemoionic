import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  menu: any[] = [
    { text: 'Accueil', url: '/home', icon: 'home' },
    { text: 'Panier', url: '/cart', icon: 'cart' },
    { text: 'A Propos', url: '/about', icon: 'book' },
  ]

  constructor(
    private menuCtrl: MenuController
  ) {}

  closeMenu() {
    this.menuCtrl.close();
  }
}
