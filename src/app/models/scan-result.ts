export interface ScanResult {
  code: string;
  product: Product;
  status: number;
  status_verbose: string;
}

interface Product {
  _id: string;
  _keywords: string[];
  added_countries_tags: any[];
  additives_debug_tags: any[];
  additives_n: number;
  additives_old_n: number;
  additives_old_tags: string[];
  additives_original_tags: string[];
  additives_prev_original_tags: string[];
  additives_tags: string[];
  additives_tags_n?: any;
  allergens: string;
  allergens_from_ingredients: string;
  allergens_from_user: string;
  allergens_hierarchy: string[];
  allergens_tags: string[];
  amino_acids_prev_tags: any[];
  amino_acids_tags: any[];
  brand_owner: string;
  brand_owner_imported: string;
  brands: string;
  brands_debug_tags: any[];
  brands_tags: string[];
  categories: string;
  categories_hierarchy: string[];
  categories_imported: string;
  categories_lc: string;
  categories_old: string;
  categories_properties: Categoriesproperties;
  categories_properties_tags: string[];
  categories_tags: string[];
  category_properties: Categoriesproperties;
  checkers: any[];
  checkers_tags: any[];
  ciqual_food_name_tags: string[];
  cities_tags: any[];
  code: string;
  codes_tags: string[];
  compared_to_category: string;
  complete: number;
  completed_t: number;
  completeness: number;
  correctors: string[];
  correctors_tags: string[];
  countries: string;
  countries_debug_tags: any[];
  countries_hierarchy: string[];
  countries_imported: string;
  countries_lc: string;
  countries_tags: string[];
  created_t: number;
  creator: string;
  data_quality_bugs_tags: any[];
  data_quality_errors_tags: any[];
  data_quality_info_tags: string[];
  data_quality_tags: string[];
  data_quality_warnings_tags: string[];
  data_sources: string;
  data_sources_imported: string;
  data_sources_tags: string[];
  debug_param_sorted_langs: string[];
  ecoscore_data: Ecoscoredata;
  ecoscore_grade: string;
  ecoscore_tags: string[];
  editors: string[];
  editors_tags: string[];
  emb_codes: string;
  emb_codes_20141016: string;
  emb_codes_debug_tags: any[];
  emb_codes_orig: string;
  emb_codes_tags: any[];
  entry_dates_tags: string[];
  expiration_date: string;
  expiration_date_debug_tags: any[];
  'fruits-vegetables-nuts_100g_estimate': number;
  generic_name: string;
  generic_name_en: string;
  generic_name_en_debug_tags: any[];
  id: string;
  image_front_small_url: string;
  image_front_thumb_url: string;
  image_front_url: string;
  image_ingredients_small_url: string;
  image_ingredients_thumb_url: string;
  image_ingredients_url: string;
  image_nutrition_small_url: string;
  image_nutrition_thumb_url: string;
  image_nutrition_url: string;
  image_small_url: string;
  image_thumb_url: string;
  image_url: string;
  images: Images;
  informers: string[];
  informers_tags: string[];
  ingredients: Ingredient[];
  ingredients_analysis_tags: string[];
  ingredients_debug: (null | string)[];
  ingredients_from_or_that_may_be_from_palm_oil_n: number;
  ingredients_from_palm_oil_n: number;
  ingredients_from_palm_oil_tags: any[];
  ingredients_hierarchy: string[];
  ingredients_ids_debug: string[];
  ingredients_n: number;
  ingredients_n_tags: string[];
  ingredients_original_tags: string[];
  ingredients_percent_analysis: number;
  ingredients_tags: string[];
  ingredients_text: string;
  ingredients_text_debug: string;
  ingredients_text_en: string;
  ingredients_text_en_debug_tags: any[];
  ingredients_text_en_imported: string;
  ingredients_text_with_allergens: string;
  ingredients_text_with_allergens_en: string;
  ingredients_that_may_be_from_palm_oil_n: number;
  ingredients_that_may_be_from_palm_oil_tags: any[];
  ingredients_with_specified_percent_n: number;
  ingredients_with_specified_percent_sum: number;
  ingredients_with_unspecified_percent_n: number;
  ingredients_with_unspecified_percent_sum: number;
  interface_version_created: string;
  interface_version_modified: string;
  known_ingredients_n: number;
  labels: string;
  labels_hierarchy: string[];
  labels_lc: string;
  labels_old: string;
  labels_tags: string[];
  lang: string;
  lang_debug_tags: any[];
  languages: Languages;
  languages_codes: Languagescodes;
  languages_hierarchy: string[];
  languages_tags: string[];
  last_edit_dates_tags: string[];
  last_editor: string;
  last_image_dates_tags: string[];
  last_image_t: number;
  last_modified_by: string;
  last_modified_t: number;
  lc: string;
  lc_imported: string;
  link: string;
  link_debug_tags: any[];
  main_countries_tags: any[];
  manufacturing_places: string;
  manufacturing_places_debug_tags: any[];
  manufacturing_places_tags: any[];
  max_imgid: string;
  minerals_prev_tags: any[];
  minerals_tags: any[];
  misc_tags: string[];
  new_additives_n: number;
  no_nutrition_data: string;
  nova_group: number;
  nova_group_debug: string;
  nova_groups: string;
  nova_groups_tags: string[];
  nucleotides_prev_tags: any[];
  nucleotides_tags: any[];
  nutrient_levels: Nutrientlevels;
  nutrient_levels_tags: string[];
  nutriments: Nutriments;
  nutriscore_data: Nutriscoredata;
  nutriscore_grade: string;
  nutriscore_score: number;
  nutriscore_score_opposite: number;
  nutrition_data: string;
  nutrition_data_per: string;
  nutrition_data_per_debug_tags: any[];
  nutrition_data_per_imported: string;
  nutrition_data_prepared: string;
  nutrition_data_prepared_per: string;
  nutrition_data_prepared_per_debug_tags: any[];
  nutrition_data_prepared_per_imported: string;
  nutrition_grade_fr: string;
  nutrition_grades: string;
  nutrition_grades_tags: string[];
  nutrition_score_beverage: number;
  nutrition_score_warning_fruits_vegetables_nuts_estimate_from_ingredients: number;
  nutrition_score_warning_fruits_vegetables_nuts_estimate_from_ingredients_value: number;
  origins: string;
  origins_hierarchy: string[];
  origins_lc: string;
  origins_old: string;
  origins_tags: string[];
  other_nutritional_substances_tags: any[];
  packaging: string;
  packaging_debug_tags: any[];
  packaging_tags: string[];
  packagings: Packaging3[];
  photographers: string[];
  photographers_tags: string[];
  pnns_groups_1: string;
  pnns_groups_1_tags: string[];
  pnns_groups_2: string;
  pnns_groups_2_tags: string[];
  popularity_key: number;
  popularity_tags: string[];
  product_name: string;
  product_name_en: string;
  product_name_en_debug_tags: any[];
  product_name_en_imported: string;
  product_quantity: string;
  purchase_places: string;
  purchase_places_debug_tags: any[];
  purchase_places_tags: any[];
  quantity: string;
  quantity_debug_tags: any[];
  removed_countries_tags: any[];
  rev: number;
  scans_n: number;
  selected_images: Selectedimages;
  serving_quantity: string;
  serving_size: string;
  serving_size_debug_tags: any[];
  serving_size_imported: string;
  sortkey: number;
  sources: Source[];
  sources_fields: Sourcesfields;
  states: string;
  states_hierarchy: string[];
  states_tags: string[];
  stores: string;
  stores_debug_tags: any[];
  stores_tags: any[];
  traces: string;
  traces_debug_tags: any[];
  traces_from_ingredients: string;
  traces_from_user: string;
  traces_hierarchy: string[];
  traces_tags: string[];
  unique_scans_n: number;
  unknown_ingredients_n: number;
  unknown_nutrients_tags: any[];
  update_key: string;
  vitamins_prev_tags: any[];
  vitamins_tags: any[];
}

interface Sourcesfields {
  'org-database-usda': Orgdatabaseusda;
}

interface Orgdatabaseusda {
  available_date: string;
  fdc_category: string;
  fdc_data_source: string;
  fdc_id: string;
  modified_date: string;
  publication_date: string;
}

interface Source {
  fields: string[];
  id: string;
  images: any[];
  import_t: number;
  url?: string;
  manufacturer?: any;
  name?: string;
}

interface Selectedimages {
  front: Front2;
  ingredients: Front2;
  nutrition: Front2;
}

interface Front2 {
  display: Display;
  small: Display;
  thumb: Display;
}

interface Display {
  en: string;
}

interface Packaging3 {
  shape: string;
}

interface Nutriscoredata {
  energy: number;
  energy_points: number;
  energy_value: number;
  fiber: number;
  fiber_points: number;
  fiber_value: number;
  fruits_vegetables_nuts_colza_walnut_olive_oils: number;
  fruits_vegetables_nuts_colza_walnut_olive_oils_points: number;
  fruits_vegetables_nuts_colza_walnut_olive_oils_value: number;
  grade: string;
  is_beverage: number;
  is_cheese: number;
  is_fat: number;
  is_water: number;
  negative_points: number;
  positive_points: number;
  proteins: number;
  proteins_points: number;
  proteins_value: number;
  saturated_fat: number;
  saturated_fat_points: number;
  saturated_fat_ratio: number;
  saturated_fat_ratio_points: number;
  saturated_fat_ratio_value: number;
  saturated_fat_value: number;
  score: number;
  sodium: number;
  sodium_points: number;
  sodium_value: number;
  sugars: number;
  sugars_points: number;
  sugars_value: number;
}

interface Nutriments {
  calcium: number;
  calcium_100g: number;
  calcium_serving: number;
  calcium_unit: string;
  calcium_value: number;
  carbohydrates: number;
  carbohydrates_100g: number;
  carbohydrates_serving: number;
  carbohydrates_unit: string;
  carbohydrates_value: number;
  cholesterol: number;
  cholesterol_100g: number;
  cholesterol_serving: number;
  cholesterol_unit: string;
  cholesterol_value: number;
  energy: number;
  'energy-kcal': number;
  'energy-kcal_100g': number;
  'energy-kcal_serving': number;
  'energy-kcal_unit': string;
  'energy-kcal_value': number;
  energy_100g: number;
  energy_serving: number;
  energy_unit: string;
  energy_value: number;
  fat: number;
  fat_100g: number;
  fat_serving: number;
  fat_unit: string;
  fat_value: number;
  fiber: number;
  fiber_100g: number;
  fiber_serving: number;
  fiber_unit: string;
  fiber_value: number;
  'fruits-vegetables-nuts-estimate-from-ingredients_100g': number;
  iron: number;
  iron_100g: number;
  iron_serving: number;
  iron_unit: string;
  iron_value: number;
  'nova-group': number;
  'nova-group_100g': number;
  'nova-group_serving': number;
  'nutrition-score-fr': number;
  'nutrition-score-fr_100g': number;
  proteins: number;
  proteins_100g: number;
  proteins_serving: number;
  proteins_unit: string;
  proteins_value: number;
  salt: number;
  salt_100g: number;
  salt_serving: number;
  salt_unit: string;
  salt_value: number;
  'saturated-fat': number;
  'saturated-fat_100g': number;
  'saturated-fat_serving': number;
  'saturated-fat_unit': string;
  'saturated-fat_value': number;
  sodium: number;
  sodium_100g: number;
  sodium_serving: number;
  sodium_unit: string;
  sodium_value: number;
  sugars: number;
  sugars_100g: number;
  sugars_serving: number;
  sugars_unit: string;
  sugars_value: number;
  'trans-fat': number;
  'trans-fat_100g': number;
  'trans-fat_serving': number;
  'trans-fat_unit': string;
  'trans-fat_value': number;
  'vitamin-a': number;
  'vitamin-a_100g': number;
  'vitamin-a_serving': number;
  'vitamin-a_unit': string;
  'vitamin-a_value': number;
  'vitamin-c': number;
  'vitamin-c_100g': number;
  'vitamin-c_serving': number;
  'vitamin-c_unit': string;
  'vitamin-c_value': number;
}

interface Nutrientlevels {
  fat: string;
  salt: string;
  'saturated-fat': string;
  sugars: string;
}

interface Languagescodes {
  en: number;
}

interface Languages {
  'en:english': number;
}

interface Ingredient {
  has_sub_ingredients?: string;
  id: string;
  percent_estimate: number;
  percent_max: number;
  percent_min: number;
  rank?: number;
  text: string;
  vegan?: string;
  vegetarian?: string;
  processing?: string;
}

interface Images {
  '1': _1;
  '2': _1;
  '3': _1;
  '4': _1;
  '5': _1;
  front: Front;
  front_en: Front;
  ingredients: Ingredients;
  ingredients_en: Ingredients;
  nutrition: Ingredients;
  nutrition_en: Ingredients;
}

interface Ingredients {
  geometry: string;
  imgid: string;
  normalize: string;
  rev: string;
  sizes: Sizes2;
  white_magic?: any;
}

interface Front {
  geometry: string;
  imgid: string;
  normalize?: any;
  rev: string;
  sizes: Sizes2;
  white_magic?: any;
}

interface Sizes2 {
  '100': _100;
  '200': _100;
  '400': _100;
  full: _100;
}

interface _1 {
  sizes: Sizes;
  uploaded_t: number;
  uploader: string;
}

interface Sizes {
  '100': _100;
  '400': _100;
  full: _100;
}

interface _100 {
  h: number;
  w: number;
}

interface Ecoscoredata {
  adjustments: Adjustments;
  agribalyse: Agribalyse;
  missing: Missing;
  missing_agribalyse_match_warning: number;
  status: string;
}

interface Missing {
  agb_category: number;
  labels: number;
  packagings: number;
}

interface Agribalyse {
  warning: string;
}

interface Adjustments {
  origins_of_ingredients: Originsofingredients;
  packaging: Packaging2;
  production_system: Productionsystem;
  threatened_species: Categoriesproperties;
}

interface Productionsystem {
  labels: any[];
  value: number;
  warning: string;
}

interface Packaging2 {
  non_recyclable_and_non_biodegradable_materials: number;
  packagings: Packaging[];
  score: number;
  value: number;
  warning: string;
}

interface Packaging {
  ecoscore_material_score: number;
  ecoscore_shape_ratio: string;
  material: string;
  shape: string;
}

interface Originsofingredients {
  aggregated_origins: Aggregatedorigin[];
  epi_score: number;
  epi_value: number;
  origins_from_origins_field: string[];
  transportation_scores: Transportationscores;
  transportation_values: Transportationscores;
  values: Transportationscores;
}

interface Transportationscores {
  ad: number;
  al: number;
  at: number;
  ax: number;
  ba: number;
  be: number;
  bg: number;
  ch: number;
  cy: number;
  cz: number;
  de: number;
  dk: number;
  dz: number;
  ee: number;
  eg: number;
  es: number;
  fi: number;
  fo: number;
  fr: number;
  gg: number;
  gi: number;
  gr: number;
  hr: number;
  hu: number;
  ie: number;
  il: number;
  im: number;
  is: number;
  it: number;
  je: number;
  lb: number;
  li: number;
  lt: number;
  lu: number;
  lv: number;
  ly: number;
  ma: number;
  mc: number;
  md: number;
  me: number;
  mk: number;
  mt: number;
  nl: number;
  no: number;
  pl: number;
  ps: number;
  pt: number;
  ro: number;
  rs: number;
  se: number;
  si: number;
  sj: number;
  sk: number;
  sm: number;
  sy: number;
  tn: number;
  tr: number;
  ua: number;
  uk: number;
  us: number;
  va: number;
  xk: number;
}

interface Aggregatedorigin {
  origin: string;
  percent: number;
}

interface Categoriesproperties {
}