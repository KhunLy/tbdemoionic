import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { from, Observable, of } from 'rxjs';
import { finalize, mergeMap } from 'rxjs/operators';
import { LoadingController } from '@ionic/angular';

@Injectable()
export class LoadingInterceptor implements HttpInterceptor {

  constructor(
    private loadingCtrl: LoadingController
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return from(this.loadingCtrl.create({
      message: 'Chargement en cours',
    })).pipe(mergeMap(l => {
      return of(l.present())
    }), mergeMap(() => {
      return next.handle(request);
    }), finalize(() => {
      this.loadingCtrl.dismiss()
    }));
  }
}

