import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ScanResult } from '../models/scan-result';

@Injectable({
  providedIn: 'root'
})
export class FoodService {

  constructor(
    private http : HttpClient
  ) { }

  get(barCode: string): Observable<ScanResult> {
    return this.http.get<ScanResult>(`https://world.openfoodfacts.org/api/v0/product/${barCode}.json`)
  }
}
