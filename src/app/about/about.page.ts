import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@awesome-cordova-plugins/barcode-scanner/ngx';
import { ScanResult } from '../models/scan-result';
import { FoodService } from '../services/food.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {

  result: ScanResult;

  constructor(
    private scanner: BarcodeScanner,
    private foodService: FoodService,
  ) { }

  ngOnInit() {
  }

  scan() {
    this.scanner.scan()
      .then(result => {
        this.foodService.get(result.text).subscribe(data => this.result = data);
      }).catch(() => {
        let value = prompt('Entrez le code barre manuellement');
        this.foodService.get(value).subscribe(data => this.result = data);
      });
  }

}
